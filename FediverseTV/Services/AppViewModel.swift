//
//  AppViewModel.swift
//  FediverseTV
//
//  Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI


/// The view model responsible for managing data and business logic for the FediverseTV app.
class AppViewModel: ObservableObject {
    /// Observable variable to control the display of the splash screen.
    @Published var showSplash = true
    /// Array of video data fetched from the API.
    @Published var videos: [VideoData] = []
    /// Array of locally stored video data.
    @Published var videoslocal: [VideoData] = []
    /// Array of video data obtained from search.
    @Published var videosSearch: [VideoData] = []
    
    /// Current page number for pagination.
    var currentPage = 1
    /// Flag to track whether last videos were called.
    var didCallLastVideos = false
    /// Flag to track whether anarchy was called.
    var didCallAnarchy = false
    /// Flag to track whether local videos were called.
    var didCallLocal  = false
    /// Array of search results.
    var searchText = ""
    /// Array of search results.
    var search: [Video] = []
    /// Selected tab index.
    var selectedTab = UserDefaults.standard.integer(forKey: "selectedTab")
    /// Selected category index.
    var selectedCategory: Int? = 0
    //
    var start = 0
    var startLocal = 0
    
    
    /// Shared observable object instance to access from any part of the application.
    static let shared = AppViewModel()
    
    /// Private initializer to prevent direct instances of AppViewModel.
    private init() {}
    /// Fetches the latest videos from the API.
    /// Fetches the latest videos from the API.
    func lastVideos(completion: @escaping () -> Void){
        let apiUrl = "https://fediverse.tv/api/v1/videos?start=0&count=25"
        
        guard let url = URL(string: apiUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }
            
            guard let jsonData = data else { return }
            
            do {
                let videoResponse = try JSONDecoder().decode(VideoResponse.self, from: jsonData)
                
                // Filtrar los videos para mostrar solo aquellos cuya etiqueta de licencia no sea "Unknown" ni "Public Domain Dedication"
                let filteredVideos = videoResponse.data.filter { video in
                    //return video.licence.label != "Unknown" && video.licence.label != "Public Domain Dedication"
                    return video.licence.label != "APPLE"
                }
                
                if filteredVideos.isEmpty {
                    // No more pages available or no videos matching the filter
                    return
                }
                
                DispatchQueue.main.async {
                    self.videos.append(contentsOf: filteredVideos)
                    self.currentPage += 1
                }
            } catch {
                print("Error decoding JSON: \(error)")
            }
        }.resume()
    }

    
    /// Searches for videos based on the entered query.
    @State private var isLoadingMore = false

    func searchVideos() {
        guard !isLoadingMore else {
            return // Si ya se está cargando más, no hacer nada
        }
        
        isLoadingMore = true // Marcar que se está cargando más
        
        guard let query = searchText.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            isLoadingMore = false // Marcar que se ha terminado de cargar
            return
        }
        
        if !videosSearch.isEmpty {
                  start = start + 25 // El nuevo valor de start será la cantidad actual de videos
              }
        
        let count = 25
        let apiUrl = "https://fediverse.tv/api/v1/videos?start=\(start)&count=\(count)&search=\(query)"
        
        guard let url = URL(string: apiUrl) else {
            isLoadingMore = false // Marcar que se ha terminado de cargar
            return
        }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print("Error searching videos: \(error)")
                self.isLoadingMore = false // Marcar que se ha terminado de cargar
                return
            }
            
            guard let data = data else {
                print("No data received")
                self.isLoadingMore = false // Marcar que se ha terminado de cargar
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let videosResponse = try decoder.decode(VideoResponse.self, from: data)
                
                // Filtrar los videos para mostrar solo aquellos cuya etiqueta de licencia no sea "Unknown"
                let filteredVideos = videosResponse.data.filter { video in
                    //return video.licence.label != "Unknown" && video.licence.label != "Public Domain Dedication"
                    return video.licence.label != "APPLE"
                }
                
                DispatchQueue.main.async {
                   
                    self.videosSearch.append(contentsOf: filteredVideos) // Agregar los nuevos videos al final de la lista
                    self.isLoadingMore = false // Marcar que se ha terminado de cargar
                }
            } catch {
                print("Error decoding JSON: \(error)")
                self.isLoadingMore = false // Marcar que se ha terminado de cargar
            }
        }.resume()
    }



    /// Fetches locally stored videos from the API.
    func localVideos() {
        
        if !videoslocal.isEmpty {
            startLocal = startLocal + 25 // El nuevo valor de start será la cantidad actual de videos
              }
        
        let apiUrl = "https://fediverse.tv/api/v1/videos?start=\(startLocal)&count=25&isLocal=true"
        
        guard let url = URL(string: apiUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }
            
            guard let jsonData = data else { return }
            
            do {
                let videoResponse = try JSONDecoder().decode(VideoResponse.self, from: jsonData)
                
                // Filtrar los videos para mostrar solo aquellos cuya etiqueta de licencia no sea "Unknown" ni "Public Domain Dedication"
                let filteredVideos = videoResponse.data.filter { video in
                    //For Apple Version
                    //return video.licence.label != "Unknown" && video.licence.label != "Public Domain Dedication"
                    return video.licence.label != "APPLE"
                }
                
                if filteredVideos.isEmpty {
                    // No more pages available or no videos matching the filter
                    return
                }
                
                DispatchQueue.main.async {
                    self.videoslocal.append(contentsOf: filteredVideos)
                    self.currentPage += 1
                }
            } catch {
                print("Error decoding JSON: \(error)")
            }
        }.resume()
    }

}
