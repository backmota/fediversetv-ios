//
//  SplashScreenView.swift
//  FediverseTV
//
//  Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// View representing the splash screen of the FediverseTV app.
struct SplashScreenView: View {
    /// Duration of the splash screen in seconds.
    let splashDuration: Double = 2.0 // 2 seconds
    
    var body: some View {
        ZStack {
            // Background color of the splash screen
            Color.white // You can change the background color according to your design
            
            // Logo in the center of the splash screen
            Image("fediverse") // Make sure "logo" is the correct name of your image in Assets
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 200, height: 200) // Adjust the size of the logo as needed
        }
        .edgesIgnoringSafeArea(.all)
        .onAppear {
            // Delay the transition to the main view after the specified time
            DispatchQueue.main.asyncAfter(deadline: .now() + splashDuration) {
                // Switch to the main view of your app (can be ContentView or another view)
                // Here, I assume you have a variable in your app that controls navigation
                // You can replace "AppViewModel.shared.showSplash = false" with your actual navigation logic
                AppViewModel.shared.showSplash = false
            }
        }
    }
}

