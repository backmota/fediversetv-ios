//
// CategoryDetailView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view that displays videos related to a specific category.
struct CategoryDetailView: View {
    /// The key of the category to display videos for.
    let categoryKey: String
    
    /// The list of videos related to the category.
    @State private var videosForCategory: [VideoData] = []
    
    /// The current page of loaded videos.
    @State private var currentPage = 1
    
    /// A flag indicating whether videos for the category have been fetched.
    @State private var didCallVideosForCategory = false
    
    @State private var startCategory = 0
    
    var body: some View {
        List {
            ForEach(videosForCategory, id: \.id) { video in
                NavigationLink(destination: VideoDetailView(video: video)) {
                    VStack(spacing: 2) {
                        // Display the thumbnail image of the video
                        AsyncImageView(urlString: "https://fediverse.tv" + video.thumbnailPath)
                            .frame(width: 350, height: 200)
                            .cornerRadius(8)
                            .shadow(radius: 3)
                        
                        // Display the name of the video
                        Text(video.name)
                            .font(.headline)
                        Text("Licencia:" + video.licence.label)
                            .font(.subheadline)
                    }
                }
                .buttonStyle(PlainButtonStyle())
                .onAppear {
                    // Detect when the last item in the list appears
                    if video == self.videosForCategory.last {
                        startCategory = startCategory + 25
                        self.loadVideosForCategory()
                    }
                }
            }
        }
        .listStyle(InsetListStyle())
        .navigationTitle(categoriesDirectory[categoryKey] ?? "Category")
        .onAppear {
            
            if videosForCategory.isEmpty {
                loadVideosForCategory()
            }
            
        }
    }
    
    
    
    /// Fetches videos related to the specified category from the API.
    func loadVideosForCategory() {
        
        let apiUrl = "https://fediverse.tv/api/v1/videos?start=\(startCategory)&count=25&categoryOneOf=\(categoryKey)"
        
        guard let url = URL(string: apiUrl) else { return }
        
        URLSession.shared.dataTask(with: url) { data, _, error in
            if let error = error {
                print("Error fetching data: \(error)")
                return
            }
            
            guard let jsonData = data else { return }
            
            do {
                let videoResponse = try JSONDecoder().decode(VideoResponse.self, from: jsonData)
                
                // Filtrar los videos para mostrar solo aquellos cuya etiqueta de licencia no sea "Unknown" ni "Public Domain Dedication"
                let filteredVideos = videoResponse.data.filter { video in
                    //return video.licence.label != "Unknown" && video.licence.label != "Public Domain Dedication"
                    return video.licence.label != "APPLE"
                }
                
                if filteredVideos.isEmpty {
                    // No more pages available or no videos matching the filter
                    return
                }
                
                DispatchQueue.main.async {
                    videosForCategory.append(contentsOf: filteredVideos)
                    currentPage += 1
                }
            } catch {
                print("Error decoding JSON: \(error)")
            }
        }.resume()
    }
}

