//
// SearchView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view for searching and displaying videos based on search criteria.
struct SearchView: View {
    /// The view model shared across the application.
    @ObservedObject var viewModel = AppViewModel.shared
    
    var body: some View {
        NavigationView {
            VStack {
                // Search bar for entering search queries
                TextField("Buscar videos", text: $viewModel.searchText, onCommit: {
                           viewModel.videosSearch.removeAll() // Eliminar todos los videos al presionar Enter
                           viewModel.start = 0
                           viewModel.searchVideos() // Llamar a la función de búsqueda
                       })
                       .textFieldStyle(RoundedBorderTextFieldStyle())
                       .padding()
                
                List {
                    ForEach(viewModel.videosSearch, id: \.id) { video in
                        // Display each video as a navigation link to its detail view
                        NavigationLink(destination: VideoDetailView(video: video)) {
                            VStack(spacing: 3) {
                                // Display the video thumbnail using an AsyncImageView
                                AsyncImageView(urlString: "https://fediverse.tv" + video.thumbnailPath)
                                    .frame(width: 350, height: 200) // Use screen width for image size
                                    .cornerRadius(8)
                                    .shadow(radius: 3)
                                
                                // Display the video name
                                Text(video.name)
                                    .font(.headline)
                                
                                Text("Licencia:" + video.licence.label)
                                    .font(.subheadline)
                            }
                        }
                        .buttonStyle(PlainButtonStyle())
                        
                        .onAppear {
                                                    // Detect when the last item in the list appears
                                                    if video == self.viewModel.videosSearch.last {
                                                        self.viewModel.searchVideos()
                                                    }
                                                }
                         
                    }
                }
                .listStyle(PlainListStyle())
            }
            .navigationTitle("Buscar Videos") // Set navigation title
            .onAppear {
                // Call searchVideos() only the first time the view appears
                if viewModel.videosSearch.isEmpty {
                    viewModel.searchVideos()
                }
            }
        }
    }
}

