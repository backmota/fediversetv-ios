//
// VideoDetailView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import AVKit
import AVFoundation
import SwiftUI

/// A view that displays details of a video, including its name, description, embedded video player, and sharing options.
struct VideoDetailView: View {
    /// The video data to display.
    let video: VideoData
    
    /// A flag indicating whether an error occurred while loading the video.
    @State private var errorOccurred = false
    
    /// A flag indicating whether the share sheet is showing.
    @State private var isShareSheetShowing = false
    
    /// The AVPlayer instance for playing the video.
    @State private var player: AVPlayer?
    
    var body: some View {
        VStack {
            // Display the video name
            Text(video.name)
                .font(.title)
            
            // Display the video description
            Text(video.description ?? "")
                .font(.body)
            
            // Display the embedded video player using a WebView
            WebView(urlString: "https://fediverse.tv" + video.embedPath)
            
            if errorOccurred {
                // Show an error message if video loading fails
                Text("Failed to load video. Please check the video URL and try again.")
                    .foregroundColor(.red)
                    .padding(.top)
            }
            
            Text("Licencia: " + video.licence.label)
                .font(.body)
            
            // Button to share the video
            Button(action: {
                isShareSheetShowing.toggle()
            }) {
                Text("Compartir")
                    .padding()
                    .foregroundColor(.white)
                    .background(Color.blue)
                    .cornerRadius(8)
            }
            .sheet(isPresented: $isShareSheetShowing) {
                ActivityView(activityItems: [URL(string: video.url)!])
            }
        }
        .padding()
        .onAppear {
            playVideo() // Auto-play the video when the view appears
        }
        .onDisappear {
            player?.pause() // Pause the video when the view disappears
        }
        .onReceive(NotificationCenter.default.publisher(for: UIApplication.willResignActiveNotification)) { _ in
            player?.play() // Resume playing the video when the app becomes inactive
        }
    }
}

extension VideoDetailView {
    /// Plays the video using AVPlayer.
    private func playVideo() {
        guard let videoURL = URL(string: "https://fediverse.tv" + video.embedPath) else { return }
        let playerItem = AVPlayerItem(url: videoURL)
        let player = AVPlayer(playerItem: playerItem)
        self.player = player
        player.play()
    }
}

