//
// LocalView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view displaying locally stored videos.
struct LocalView: View {
    /// The view model shared across the application.
    @ObservedObject var viewModel = AppViewModel.shared
        
    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.videoslocal, id: \.id) { video in
                    // Display each video as a navigation link to its detail view
                    NavigationLink(destination: VideoDetailView(video: video)) {
                        VStack(spacing: 2) {
                            // Display the video thumbnail using an AsyncImageView
                            AsyncImageView(urlString: "https://fediverse.tv" + video.thumbnailPath)
                                .frame(width: 350, height: 200) // Use screen width for image size
                                .cornerRadius(8)
                                .shadow(radius: 3)
                            
                            // Display the video name
                            Text(video.name)
                                .font(.headline)
                            Text("Licencia:" + video.licence.label)
                                .font(.subheadline)
                                .onAppear {
                                                            // Detect when the last item in the list appears
                                    if video == self.viewModel.videoslocal.last {
                                        self.viewModel.localVideos()
                                                            }
                                                        }
                        }
                    }
                    .buttonStyle(PlainButtonStyle())
                }
            }
            .listStyle(InsetListStyle())
            .navigationTitle("Local") // Set navigation title
            .onAppear {
                // Call localVideos() only the first time the view appears
                if viewModel.videoslocal.isEmpty {
                    viewModel.localVideos()
                }
               
            }
        }
    }
}

