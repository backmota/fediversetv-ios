//
// AboutView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view displaying information about the application and its developers.
struct AboutView: View {
    /// The view model shared across the application.
    @ObservedObject var viewModel = AppViewModel.shared
    
    var body: some View {
        NavigationView {
            VStack {
                // Add image from assets before the first text
                Image("fediverse") // Make sure the name matches your image's name in assets
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 100, height: 100) // Adjust image size as needed
                    .padding(.bottom, 8) // Add space below the image
                
                Text("¿Quiénes somos?").bold()
                    .padding(.bottom, 8) // Add space below the text
                
                Text("Fediverse.tv es un colectivo activista dedicado a las comunicaciones libres y soberanas")
                    .padding(.bottom, 16) // Add space below the text
                
                // Add a link to an external website within the VStack
                Link("Ir a fediverse.tv", destination: URL(string: "https://fediverse.tv/")!)
                    .foregroundColor(.blue) // Link text color
                    .padding(.bottom, 8) // Add space below the link
                
                Link("Blog", destination: URL(string: "https://blog.fediverse.tv/manifiesto/")!)
                    .foregroundColor(.blue) // Link text color
                    .padding(.bottom, 16) // Add space below the link
                
                Text("Términos y condiciones").bold()
                    .padding(.bottom, 8) // Add space below the text
                
                Link("https://blog.fediverse.tv/condiciones-y-terminos-de-uso/", destination: URL(string: "https://blog.fediverse.tv/condiciones-y-terminos-de-uso/")!)
                    .foregroundColor(.blue)
                    .padding(.bottom, 16) // Add space below the link
                
                Text("App desarrollada por @backmota").bold().lineLimit(nil)
                    .padding(.bottom, 8) // Add space below the text
                
                Link("https://mastodon.social/@backmota", destination: URL(string: "https://mastodon.social/@backmota")!)
                    .foregroundColor(.blue)
                    .padding(.bottom, 16) // Add space below the link
            }
            .padding() // Add space around the entire VStack
            .navigationBarTitle("FediverseTV") // Title for the main navigation bar
        }
    }
}
