//
// AsyncImageView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view for asynchronously loading and displaying images from a URL.
struct AsyncImageView: View {
    /// The image loader responsible for fetching and caching the image from the specified URL.
    @StateObject private var imageLoader: ImageLoader
    
    /// The placeholder image to display while the actual image is being loaded.
    let placeholderImage: Image = Image(systemName: "photo") // You can change this image to one of your choice
    
    /// Initializes an async image view with the specified URL string.
    /// - Parameter urlString: The URL string from which to load the image.
    init(urlString: String) {
        _imageLoader = StateObject(wrappedValue: ImageLoader(urlString: urlString))
    }
    
    var body: some View {
        if let image = imageLoader.image {
            Image(uiImage: image)
                .resizable()
                .scaledToFit()
        } else {
            placeholderImage
                .resizable()
                .scaledToFit()
        }
    }
}

