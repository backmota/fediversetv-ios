//
// ActivityView.swift
// FediverseTV
//
// Created by José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view for presenting an activity view controller to share content.
struct ActivityView: UIViewControllerRepresentable {
    /// The items to share in the activity view controller.
    let activityItems: [Any]
    
    /// Creates a UIActivityViewController with the specified activity items.
    /// - Parameter context: The context for the view's creation.
    /// - Returns: A configured UIActivityViewController instance.
    func makeUIViewController(context: Context) -> UIActivityViewController {
        let controller = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        return controller
    }
    
    /// Updates the UIActivityViewController.
    /// - Parameters:
    ///   - uiViewController: The UIActivityViewController to update.
    ///   - context: The context for the view's updating.
    func updateUIViewController(_ uiViewController: UIActivityViewController, context: Context) {
        // No need to update
    }
}

