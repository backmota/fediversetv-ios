//
// CategoriesView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// A view displaying categories available in the application.
struct CategoriesView: View {
    /// The view model shared across the application.
    @ObservedObject var viewModel = AppViewModel.shared
    
    var body: some View {
        NavigationView {
            List {
                // Display each category as a navigation link to its detail view
                ForEach(categoriesDirectory.sorted(by: { $0.key < $1.key }), id: \.key) { key, value in
                    NavigationLink(destination: CategoryDetailView(categoryKey: key)) {
                        Text("\(value)") // Display the category name
                    }
                }
            }
            .navigationTitle("Categorías") // Set navigation title
        }
    }
}

