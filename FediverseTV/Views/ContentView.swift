//
//  ContentView.swift
//  FediverseTV
//
//  Created by @backmota - José Juan Mota on 19/03/24.
//

import SwiftUI


/// The main view of the FediverseTV app, displaying different tabs for search, local videos, categories, and about information.
struct ContentView: View {
    /// The view model shared across the ContentView and its subviews.
    @ObservedObject var viewModel = AppViewModel.shared
    
    var body: some View {
        TabView(selection: $viewModel.selectedCategory) {
            // Search tab
            SearchView()
                .tabItem {
                    Label("Buscador", systemImage: "magnifyingglass")
                }
                .tag(0)
            // Local tab
            LocalView()
                .tabItem {
                    Label("Local", systemImage: "house")
                }
                .tag(1)
            // Categories tab
            CategoriesView()
            
                .tabItem {
                    Label("Categorías", systemImage: "list.bullet")
                }
                .tag(2)
            // About tab
            AboutView()
                .tabItem {
                    Label("Acerca de", systemImage: "questionmark.bubble")
                }
                .tag(3)
            
            
        }
        .onAppear {
            // Retrieve saved state when the app reappears
            viewModel.searchText = UserDefaults.standard.string(forKey: "searchText") ?? ""
        }
        .onChange(of: viewModel.selectedCategory) { newValue in
            // Save the selected tab state
            UserDefaults.standard.set(viewModel.selectedCategory, forKey: "selectedTab")
        }
        
    }
    
    
}

/// A preview provider for ContentView, used for SwiftUI previews.
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


