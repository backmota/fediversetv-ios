//
// WebView.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI
import WebKit

/// A view for displaying web content using a WKWebView.
struct WebView: UIViewRepresentable {
    /// The URL string to load in the web view.
    let urlString: String
    
    /// Creates a WKWebView and configures it with the specified URL string.
    /// - Parameter context: The context for the view's creation and configuration.
    /// - Returns: A configured WKWebView instance.
    func makeUIView(context: Context) -> WKWebView {
        let webView = WKWebView()
        return webView
    }
    
    /// Updates the WKWebView with the specified URL.
    /// - Parameters:
    ///   - uiView: The WKWebView to update.
    ///   - context: The context for the view's updating.
    func updateUIView(_ uiView: WKWebView, context: Context) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            uiView.load(request)
        } else {
            print("Error: Invalid URL")
        }
    }
}

