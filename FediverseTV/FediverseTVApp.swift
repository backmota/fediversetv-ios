//
// FediverseTVApp.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 19/03/24.
//

import SwiftUI

/// The main entry point for the FediverseTV application.
@main
struct YourAppNameApp: App {
    /// The shared view model used across the application.
    @StateObject var viewModel = AppViewModel.shared
    
    var body: some Scene {
        WindowGroup {
            if viewModel.showSplash {
                SplashScreenView() // Show the splash screen if viewModel.showSplash is true
            } else {
                ContentView() // Show the main content view if viewModel.showSplash is false
            }
        }
    }
}

