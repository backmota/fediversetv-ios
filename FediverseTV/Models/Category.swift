//
//  Category.swift
//  FediverseTV
//
//  Created by José Juan Mota on 22/03/24.
//

import SwiftUI
import Foundation

/// Represents a category object with Identifiable and Decodable conformance.
struct Category: Identifiable, Decodable {
    /// The unique identifier of the category.
    let id: String
    /// The name of the category.
    let name: String
}
