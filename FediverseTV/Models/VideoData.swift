//
//  VideoData.swift
//  FediverseTV
//
//  Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI

/// Directory mapping category IDs to category names.
let categoriesDirectory: [String: String] = [
    "1": "Música",
    "3": "Vehículos",
    "4": "Arte",
    "5": "Deportes",
    "6": "Viajes",
    "7": "Videojuegos",
    "8": "Personas",
    "9": "Comedia",
    "10": "Entretenimiento",
    "11": "Noticias y Política",
    "12": "Cómo Hacer",
    "13": "Educación",
    "14": "Activismo",
    "15": "Ciencia y Tecnología",
    "16": "Animales",
    "18": "Comida",
    "19": "Fedicorto",
    "2": "Películas",
    "44": "Sonidos de Naturaleza",
]

/// Struct representing video data fetched from the API.
struct VideoData: Codable, Identifiable, Equatable  {
    static func == (lhs: VideoData, rhs: VideoData) -> Bool {
        return lhs.id == rhs.id
    }
    
    /// Unique identifier for the video data.
    let id = UUID()
    /// URL of the video.
    let url: String
    /// Embed path of the video.
    let embedPath: String
    /// Name or title of the video.
    let name: String
    /// Description of the video (optional).
    let description: String?
    /// Path to the thumbnail image of the video.
    let thumbnailPath: String
    
    let licence: Licence
}

struct Licence: Codable {
    /// The identifier of the licence.
    let id: Int?
    
    /// The label or name of the licence.
    let label: String
}
