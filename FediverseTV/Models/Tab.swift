//
//  Tab.swift
//  FediverseTV
//
//  Created by @backmota José Juan Mota on 22/03/24.
//

import SwiftUI

/// Represents a tab in the FediverseTV application.
struct Tab {
    /// Unique identifier for the tab.
    let id = UUID()
    /// The name or title of the tab.
    let name: String
}
/// An array of predefined tabs for the FediverseTV application.
let tabs: [Tab] = [
    Tab(name: "Recientes"),     // Tab for displaying recent content
    Tab(name: "Local"),         // Tab for displaying local content
    Tab(name: "Categorías"),    // Tab for displaying categories
    Tab(name: "Acerca de")      // Tab for displaying information about the app
]
