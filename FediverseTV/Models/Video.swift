//
// Video.swift
// FediverseTV
//
// Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI
import Foundation

/// Represents a video object with Codable and Identifiable conformance.
struct Video: Codable, Identifiable {
    /// The unique identifier of the video.
    let id: String
    
    /// The name or title of the video.
    let name: String
    
    /// The description or details of the video.
    let description: String
    /// Licence  details of the video.
    let licence: String
    ////// Licence  details of the video.
    
    // Additional properties you want to fetch
}

/// Represents the response structure for video data.
struct VideoResponse: Codable {
    /// An array of VideoData objects.
    let data: [VideoData]
}
