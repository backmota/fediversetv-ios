//
//  ImageLoader.swift
//  FediverseTV
//
//  Created by @backmota - José Juan Mota on 22/03/24.
//

import SwiftUI
import Combine

/// Class responsible for loading images from a URL asynchronously.
class ImageLoader: ObservableObject {
    /// Published property for the loaded image.
    @Published var image: UIImage?
    /// The URL string from which the image will be loaded.
    private let urlString: String
    /// Cancellable object to manage the data task subscription.
    private var cancellable: AnyCancellable?
    
    /// Initializes the ImageLoader with the specified URL string and triggers the image loading process.
    /// - Parameter urlString: The URL string from which to load the image.
    init(urlString: String) {
        self.urlString = urlString
        load()
    }
    
    /// Initiates the data task to load the image from the URL.
    func load() {
        guard let url = URL(string: urlString) else { return }
        
        cancellable = URLSession.shared.dataTaskPublisher(for: url)
            .map { UIImage(data: $0.data) } // Map data to UIImage
            .replaceError(with: nil) // Replace any errors with nil
            .receive(on: DispatchQueue.main) // Receive on the main queue
            .assign(to: \.image, on: self) // Assign the loaded image to the published property
    }
}

