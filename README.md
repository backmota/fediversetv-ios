# FediverseTV iOS App

This is the iOS app for FediverseTV, a project that utilizes the Peertube API provided by the instance at [https://fediverse.tv/](https://fediverse.tv/).

## Overview

The FediverseTV iOS app allows users to browse and watch videos from the Peertube platform. It provides features such as searching for videos, viewing categories, and more.

## Features

- Browse recent videos
- Search for videos by keyword
- Explore videos by categories
- Watch videos with built-in video player
- Share videos with others

## Installation

To use the FediverseTV iOS app, follow these steps:

1. Clone the repository to your local machine.
2. Open the project in Xcode.
3. Build and run the app on your iOS device or simulator.

## Usage

Upon launching the app, you will see a list of recent videos. Use the tabs at the bottom to navigate between different sections such as recent videos, local videos, categories, and about.

## API and Instance

The FediverseTV iOS app uses the Peertube API provided by the instance at [https://fediverse.tv/](https://fediverse.tv/).

## Contributing

If you'd like to contribute to the development of the FediverseTV iOS app, please follow these guidelines:

1. Fork the repository.
2. Create a new branch (`git checkout -b feature/my-feature`).
3. Make your changes.
4. Commit your changes (`git commit -am 'Add new feature'`).
5. Push to the branch (`git push origin feature/my-feature`).
6. Create a new Pull Request.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Contact

For any inquiries or support regarding the FediverseTV iOS app, please contact [backmota.dev@proton.me](mailto:backmota.dev@proton.me).

